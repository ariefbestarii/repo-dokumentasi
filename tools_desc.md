# A. **Gitlab**
  ## 1. **Gitlab Runner**
  - CPU: 2 vCPU
  - RAM: 2 GB
  - Penyimpanan: 50 GB
  - Sistem Operasi: Ubuntu Server 20 LTS
  - Gitlab Runner Executor: Docker
  - Gitlab Runner Docker Image Executor Bawaan: Docker
  ## 2. **Gitlab CI**
  ### a. **Stage**
  - Preparation
  - Build
  - Testing
  - Review
  - Past-review
  - Release
  - Deploy
  ### b. **Job**
  - **Generate Tag**
    - Stage: Preparation
    - Rules: 
      - Berjalan di semua pipeline. 
      - Job dijalan secara manual jika berada di pipeline Merge Request. 
      - Job dijalankan otomatis jika berada di pipeline selain Merge Request. 
    - Docker Image: [Linux Alpine](https://hub.docker.com/_/alpine) (alpine:latest)
    - Tasks: 
      1. Menyalin isi dari file `./VERSION` dan membuat salinan file `./VERSION` ke file `./info.env`. 
         > Job ini menghasilkan variabel `$TAG` yang berisi versi software. 
    - Artifak: 
      - Dotenv: info.env
  - **Build Image**
    - Stage: Build
    - Rules:
      - Berjalan hanya di pipeline selain branch bawaan (master) dan Tag Release. 
      - Tidak berjalan di pipeline branch bawaan (master). 
      - Dijalan otomatis setelah job sebelumnya di pipeline selain branch bawaan (master) dan Tag Release. 
    - Depedensi:
      - Job: Generate Tag
    - Docker Image: [Docker](https://hub.docker.com/_/docker) (docker:latest)
    - Services: docker:dind
    - Tasks: 
      1. Login ke Docker Hub dengan username yang mengambil variabel $DOCKER_USER dan password yang mengambil variabel $DOCKER_PASSWORD.
          > Diperlukan *Docker socket binding* agar tidak menimbulkan error saat melakukan otentikasi ke Docker Hub. Dokumentasi Gitlab: https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-socket-binding . 
      2. Melakukan build Docker image yang terkompresi berdasar Dockerfile dengan atribut tag `$DOCKER_USER`/`$DOCKER_IMAGE_NAME`:`$TAG`. 
      3. Melakukan push pada docker image yang sebelumnya dibuat ke repositori privat di registry Docker Hub.
    - Variabel:
      - `$TAG` merupakan variabel yang berasal dari job "Generate Tag". Variabel ini berisi versi software. 
      - `$DOCKER_USER` merupakan variabel berisi username dari akun Docker Hub.
      - `$DOCKER_IMAGE_NAME` merupakan variabel berisi nama imaga sekaligus nama repositori image di Docker Hub.
      > Variabel `$DOCKER_USER`, dan `$DOCKER_IMAGE_NAME` harus ditambahkan terlebih dahulu secara manual melalui pengaturan. Dokumentasi Gitlab https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project . 
  - **Testing**
    - Stage: Testing
    - Rules:
      - Berjalan di pipeline semua branch kecuali branch bawaan (master), pipeline merge request, dan pipeline Tag Release. 
      - Tidak berjalan di pipeline branch bawaan (master). 
      - Dijalan otomatis setelah job sebelumnya di pipeline semua branch kecuali branch bawaan (master), pipeline merge request, dan pipeline Tag Release.
    - Depedensi:
      - Job: Generate Tag 
    - Docker Image: Image hasil dari Job Build Image
      > Catatan: Dibutuhkan otantikasi ke registry Docker Image (dalam hal ini Docker Hub) yang dimana proses otentikasi tersebut memerlukan variabel `$DOCKER_AUTH_CONFIG`. Dokumentasi GitLab: https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#use-statically-defined-credentials . 
    - Tasks:
      1. Menyusul
    - Artifak:
      - JUnit: report-phpunit.xml
    - Variabel:
      - `$TAG` merupakan variabel yang berasal dari job "Generate Tag". Variabel ini berisi versi software. 
      - `$DOCKER_AUTH_CONFIG` merupakan variabel yang menjadi sumber konfigurasi untuk otentikasi login ke repositori privat Docker Image.
      > `$DOCKER_AUTH_CONFIG` wajib ditambahkan terlebih dahulu secara manual melalui pengaturan untuk menghidari error saat proses CI/CD. Dokumentasi Gitlab: 
      >- https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project . 
      >- https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#use-statically-defined-credentials
  - **Review on Development Environment**
    - Stage: Review
    - Rules:
      - Berjalan hanya di pipeline selain branch bawaan (master) dan pipeline merge request.
      - Tidak berjalan di pipeline branch bawaan (master) dan pipeline tag release. 
      - Dijalankan manual setelah job sebelumnya selesai di pipeline semua branch selain branch bawaan (master) dan pipeline merge request.
    - Depedensi:
      - Job: Generate Tag
    - Docker Image: [Linux Alpine](https://hub.docker.com/_/alpine) (alpine:latest)
    - Tasks:
      1. Melakukan update terhadap package list Alpine Linux. 
      2. Menginstall package OpenSSH. 
      3. Menginstall package Rsync. 
      4. Menginstall package SSHpass. 
      5. Mengonfigurasi SSH. 
      6. Menyinkronkan file statis (css, js, dan lain-lain) ke server NFS (`$IP_NFS`) menggunakan tool SSHpass dengan memanfaatkan rsync dalam proses sinkronisasinya dan menggunakan protokol SSH dalam sinkronisasinya. 
      7. Melakukan login ke Docker Hub di Environment Development (`$IP_DEV`)menggunakan tool SSHpass yang memanfaatkan SSH dalam prosesnya.
      8. Melakukan pull image dari repositori Docker Hub di Environment Development (`$IP_DEV`) menggunakan tool SSHpass yang memanfaatkan SSH dalam prosesnya.
      9. Menjalankan image tersebut di Environment Development (`$IP_DEV`) menggunakan network dari Host, nama `development_$TAG`, dan volume dari NFS server di Environment Development menggunakan tool SSHpass yang memanfaatkan SSH dalam prosesnya. 
    - Variabel:
      - `$TAG` merupakan variabel yang berasal dari job "Generate Tag". Variabel ini berisi versi software.
      - `$IP_NFS` merupakan variabel yang berisi IP dari Server NFS.
      - `$IP_DEV` merupakan variabel yang berisi IP dari Evironment. Development.
      > Variabel `$IP_NFS` dan `$IP_DEV` harus ditambahkan terlebih dahulu secara manual melalui pengaturan. Dokumentasi Gitlab https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project . 
  - **Clean-up Development Environment**
    - Stage: Past-review
    - Rules:
      - Berjalan hanya di pipeline selain branch bawaan (master) dan pipeline merge request. 
      - Tidak berjalan di pipeline branch bawaan (master) dan pipeline tag release. 
      - Dijalankan manual setelah job sebelumnya selesai di pipeline semua branch selain branch bawaan (master) dan pipeline merge request.
    - Depedensi:
      - Job: Generate Tag
    - Docker Image: [Linux Alpine](https://hub.docker.com/_/alpine) (alpine:latest)
    - Tasks:
      1. Melakukan update terhadap package list Alpine Linux. 
      2. Menginstall package OpenSSH. 
      3. Menginstall package SSHpass. 
      4. Mengonfigurasi SSH. 
      5. Menghentikan container yang sudah dijalankan pada Environment Development (`$IP_DEV`) di job sebelumnya yang sebelumnya menggunakan nama `development_$TAG`. 
      6. Mengahapus container yang sebelumnya dihentikan di Environment Development (`$IP_DEV`). 
    - Variabel:
      - `$TAG` merupakan variabel yang berasal dari job "Generate Tag". Variabel ini berisi versi software. 
      - `$IP_DEV` merupakan variabel yang berisi IP dari Evironment. Development.
      > Variabel `$IP_DEV` harus ditambahkan terlebih dahulu secara manual melalui pengaturan. Dokumentasi Gitlab https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project . 
  - **Tag Release**
    - Stage: Deploy
    - Rules: 
      - Berjalan hanya di pipeline branch bawaan (master). 
      - Tidak berjalan di pipeline branch selain branch bawaan (master), pipeline merge request, dan pipeline tag release. 
      - Dijalankan manual setelah job sebelumnya selesai di pipeline branch bawaan (master).
    - Depedensi:
      - Job: Generate Tag
    - Docker Image: [Realease CLI](https://gitlab.com/gitlab-org/release-cli) (`registry.gitlab.com/gitlab-org/release-cli`)
    - Tasks:
      1. Menampilkan "Release v$TAG"
      2. Melakukan tag release terhadap perubahan commit terakhir dengan atribut nama: `Release v$TAG` , deskripsi yang mengambil isi dari file `./CHANGELOG`, dan nama tag: `v$TAG`
    - Variabel:
      - `$TAG` merupakan variabel yang berasal dari job "Generate Tag". Variabel ini berisi versi software. 
  - **Template Job: Deploy to Production**
      - Stage: Deploy
      - Rules:
        - Berjalan hanya di pipeline tag release
        - Dijalankan manual setelah job sebelumnya selesai di pipeline tag release.
      - Depedensi:
        - Job: Generate Tag
      - Docker Image: [Linux Alpine](https://hub.docker.com/_/alpine) (alpine:latest)
      - Tasks:
        1. Melakukan update terhadap package list Alpine Linux. 
        2. Menginstall package OpenSSH. 
        3. Menginstall package Rsync. 
        4. Menginstall package SSHpass. 
        5. Mengonfigurasi SSH.  
        6. Melakukan login ke Docker Hub di Environment Production (`$IP`)menggunakan tool SSHpass yang memanfaatkan SSH dalam prosesnya.
        7. Melakukan pull image dari repositori Docker Hub di Environment Development (`$IP`) menggunakan tool SSHpass yang memanfaatkan SSH dalam prosesnya.
        8. Menjalankan image tersebut di Environment Production (`$IP`) menggunakan network dari Host, nama `production_app`, dan volume dari NFS server di Environment Production menggunakan tool SSHpass yang memanfaatkan SSH dalam prosesnya. 
      - Variabel:
        - `$TAG` merupakan variabel yang berasal dari job "Generate Tag". Variabel ini berisi versi software.
        - `$IP_NFS` merupakan variabel yang berisi IP dari Server NFS.
          > Variabel `$IP_NFS` harus ditambahkan terlebih dahulu secara manual melalui pengaturan. Dokumentasi Gitlab https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project . 
        - `$IP` merupakan variabel yang berisi IP dari Evironment Production. 
          > Variabel `$IP` harus ditambahkan terlebih dahulu secara manual di tiap-tiap job deploy production. Dokumentasi Gitlab https://docs.gitlab.com/ee/ci/variables/#create-a-custom-cicd-variable-in-the-gitlab-ciyml-file . 
    - **Deploy to Machine 1**
      - Tasks: 
        1. Menggunakan tasks pada template Deploy to Production. 
        2. Menyinkronkan file statis (css, js, dan lain-lain) ke server NFS (`$IP_NFS`) menggunakan tool SSHpass dengan memanfaatkan rsync dalam proses sinkronisasinya dan menggunakan protokol SSH dalam sinkronisasinya. 
      - Variabel:
        - `$IP` merupakan variabel yang berisi IP Mesin 1
          > Variabel `$IP` harus ditambahkan terlebih dahulu secara manual di tiap-tiap job deploy production. Dokumentasi Gitlab https://docs.gitlab.com/ee/ci/variables/#create-a-custom-cicd-variable-in-the-gitlab-ciyml-file . 
        - `$IP_NFS` merupakan variabel yang berisi IP dari Server NFS.
          > Variabel `$IP_NFS` harus ditambahkan terlebih dahulu secara manual melalui pengaturan. Dokumentasi Gitlab https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project . 
    - **Deploy to Machine 2**
      - Tasks: 
        1. Menggunakan tasks pada template Deploy to Production. 
      - Variabel:
        - `$IP` merupakan variabel yang berisi IP Mesin 2
          > Variabel `$IP` harus ditambahkan terlebih dahulu secara manual di tiap-tiap job deploy production. Dokumentasi Gitlab https://docs.gitlab.com/ee/ci/variables/#create-a-custom-cicd-variable-in-the-gitlab-ciyml-file . 
    - **Deploy to Machine 3**
      - Tasks: 
        1. Menggunakan tasks pada template Deploy to Production. 
      - Variabel:
        - `$IP` merupakan variabel yang berisi IP Mesin 3
          > Variabel `$IP` harus ditambahkan terlebih dahulu secara manual di tiap-tiap job deploy production. Dokumentasi Gitlab https://docs.gitlab.com/ee/ci/variables/#create-a-custom-cicd-variable-in-the-gitlab-ciyml-file . 