# Create User Sudo

Jalankan pada semua VM dengan user root

* Register user to sudo group
```
usermod -aG sudo <your-name>
```
* Enable sudo without password
```
echo "<your-name> ALL=(ALL) NOPASSWD:ALL" | tee /etc/sudoers.d/<your-name>
```
* login to other user
```
su - <your-name>
```
